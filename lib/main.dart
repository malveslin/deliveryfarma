import 'package:deliveryfarma/src/pages/login.dart';
import 'package:deliveryfarma/src/pages/signup.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Delivery Farma',
      theme: ThemeData(
          primarySwatch: Colors.deepOrange,
          accentColor: Colors.indigo
      ),
      home: Login(),
      routes: {
        "about": (_) => Login(),
        "signup": (_) => SignupOnePage()
      },
    );
  }
}
