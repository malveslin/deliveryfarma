import 'package:cloud_firestore/cloud_firestore.dart';

class Farmacia {
  final String nome;
  final String foto;
  final String id;
  final GeoPoint geolocation;

  Farmacia({
    this.nome,
    this.foto,
    this.id,
    this.geolocation
  });

  Map<String, Object> toJson() {
    return {
      'nome': nome,
      'foto': foto,
      'id': id,
      'geolocation' : geolocation
    };
  }

  factory Farmacia.fromJson(Map<String, Object> doc) {
    Farmacia farmacia = new Farmacia(
      id: doc['id'],
      nome: doc['nome'],
      foto: doc['foto'],
      geolocation: doc['geolocation']
    );
    return farmacia;
  }

  factory Farmacia.fromDocument(DocumentSnapshot doc) {
    doc.data["id"]  =   doc.documentID;
    return Farmacia.fromJson(doc.data);
  }
}
