import 'dart:convert';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:crypto/crypto.dart';

class CreditCardModel {

  CreditCardModel({this.cardNumber, this.expiryDate, this.cardHolderName, this.cvvCode, this.isCvvFocused, this.brand, this.user});

  String cardNumber = '';
  String expiryDate = '';
  String cardHolderName = '';
  String cvvCode = '';
  bool isCvvFocused = false;
  String brand = '';
  String user =  '';


  Map<String, Object> toJson() {
    return {
     "cardNumber" : cardNumber,
     "expiryDate": expiryDate,
     "cvvCode": cvvCode,
     "cardHolderName": cardHolderName,
     "brand": brand,
     "user": user
    };
  }



  Map<String, Object> toJsonEncrypt() {
    return {
      encrypt64("cardNumber"): encrypt64(cardNumber),
      encrypt64("expiryDate"): encrypt64(expiryDate),
      encrypt64("cvvCode"): encrypt64(cvvCode),
      encrypt64("holderName"): encrypt64(cardHolderName),
      encrypt64("brand"): encrypt64(brand),
      encrypt64("user"): encrypt64(user)
    };
  }


  factory CreditCardModel.fromJson(Map<String, Object> doc) {
    CreditCardModel cardModel = new CreditCardModel(
        cardNumber: dcrypt64(doc[encrypt64("cardNumber")]),
        expiryDate: dcrypt64(doc[encrypt64("expiryDate")]),
        cardHolderName: dcrypt64(doc[encrypt64("holderName")]),
        cvvCode: dcrypt64(doc[encrypt64("cvvCode")]),
        brand: dcrypt64(doc[encrypt64("brand")]),
        user: dcrypt64(doc[encrypt64("user")])
    );
    return cardModel;
  }

  factory CreditCardModel.fromDocument(DocumentSnapshot doc) {
    doc.data["id"]  =   doc.documentID;
    return CreditCardModel.fromJson(doc.data);
  }


  static String encrypt64(String data){
    return  base64.encode(utf8.encode(data));
  }
  static String dcrypt64(String data){
    return data.isEmpty ? '': utf8.decode(base64.decode(data));
  }



}