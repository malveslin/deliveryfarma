import 'package:cloud_firestore/cloud_firestore.dart';

class Pedido {
  final String id;
  final int codigo;
  final String descricao;
  final DocumentReference farmacia;
  final DocumentReference user;
  Map<dynamic, dynamic> response = {};
  final Timestamp data;

  Pedido({
    this.id,
    this.codigo,
    this.descricao,
    this.farmacia,
    this.user,
    this.data,
    this.response
  });

  Map<String, Object> toJson() {
    return {
      'id': id,
      'codigo': codigo,
      'descricao': descricao,
      'farmacia': farmacia,
      'user': user,
      'data': data,
      'response': response
    };
  }

  factory Pedido.fromJson(Map<String, Object> doc) {
    Pedido pedido = new Pedido(
      id: doc['id'],
      codigo: doc['codigo'],
      descricao: doc['descricao'],
      farmacia: doc['farmacia'],
      user: doc['user'],
      data: doc['data'],
      response:  doc['response'],
    );
    return pedido;
  }

  factory Pedido.fromDocument(DocumentSnapshot doc) {
    doc.data["id"]  =   doc.documentID;
    return Pedido.fromJson(doc.data);
  }
}
