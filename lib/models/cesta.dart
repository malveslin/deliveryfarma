import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:deliveryfarma/models/produto.dart';

class Cesta {
  List<Produto> produtos;
  double frete;
  double total;
  int quantidade;
  DocumentReference farmacia;

  Cesta(){
    this.frete =  this.valorFrete() + 0.0;
    this.produtos = [];
  }


  subtotal(){
    double valor = 0;
    produtos.forEach((produto){
      valor += produto.preco * produto.qtdPedido;
    });
    return valor;
  }


  valorFrete (){
    int min = 5;
    int max = 10;
    return min + (Random(1).nextInt(max-min));
  }

  valorEmCentavos(){
    var total =  this.frete + this.subtotal();
    int centavos =  total.toInt();
    int restante =  ((total - centavos) * 100).toInt() + (total * 100).toInt();
    return restante;
  }

}