import 'package:cloud_firestore/cloud_firestore.dart';

class Produto {
  final String id;
  final String nome;
  final String marca;
  final String descricao;
  final String imagem;
  final double preco;
  final int quantidade;
  final DocumentReference farmacia;
  final DocumentReference categoria;
  int qtdPedido;

  Produto({
    this.id,
    this.nome,
    this.marca,
    this.descricao,
    this.imagem,
    this.preco,
    this.quantidade,
    this.farmacia,
    this.categoria
  });

  Map<String, Object> toJson() {
    return {
      'id': id,
      'nome': nome,
      'marca': marca,
      'decricao': descricao,
      'imagem': imagem,
      'preco': preco,
      'quantidade': quantidade,
      'farmacia': farmacia,
      'categoria': categoria
    };
  }

  factory Produto.fromJson(Map<String, Object> doc) {
    Produto produto = new Produto(
      id: doc['id'],
      nome: doc['nome'],
      marca: doc['marca'],
      descricao: doc['descricao'],
      imagem: doc['imagem'],
      preco: double.parse(doc['preco'].toString()),
      quantidade: int.parse(doc['quantidade'].toString()),
      farmacia: doc['farmacia'],
      categoria: doc['categoria']
    );
    return produto;
  }

  factory Produto.fromDocument(DocumentSnapshot doc) {
    doc.data["id"]  =   doc.documentID;
    return Produto.fromJson(doc.data);
  }
}
