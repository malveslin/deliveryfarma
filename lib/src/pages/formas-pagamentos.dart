import 'package:deliveryfarma/business/cielo.dart';
import 'package:deliveryfarma/models/creditcard.dart';
import 'package:deliveryfarma/models/produto.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import 'add-forma-pagamento.dart';

class FormaPagamentoPage extends StatefulWidget {
  FormaPagamentoPage();

  @override
  _FormaPagamentoPageState createState() => _FormaPagamentoPageState();

}

class _FormaPagamentoPageState extends State<FormaPagamentoPage> {


  final FirebaseAuth firebaseAuth = FirebaseAuth.instance;
  var currentUser = null;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    firebaseAuth.onAuthStateChanged
        .firstWhere((user) => user != null)
        .then((user) {
      setState(() {
        currentUser =  user;
      });
    });
  }

  @override
    Widget build(BuildContext context) {
      return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          elevation: 0,
          backgroundColor: Colors.blue[400],
          title: Text("Pagamento"),
        ),
        body: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text("Formas de Pagamento"),
            ),
            Container(child: _buildPageContent(context)),
          ],
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.push(context, MaterialPageRoute(
                builder: (BuildContext context) => FormaPagamentoAddPage()
            ));
          },
            backgroundColor: Colors.blue[900],
          child: Icon(Icons.add,)),
      );
    }

  Widget _buildPageContent(context) {
    return currentUser != null ? StreamBuilder<Object>(
        stream: Cielo.getFormasPagamento(currentUser.uid),
        builder: (context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.hasData) {
            return Container(
              height: MediaQuery.of(context).size.height - 200,
              width: MediaQuery.of(context).size.width,
              child: new ListView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: snapshot.data.length,
                  padding: const EdgeInsets.all(1.0),
                  itemBuilder: (context, index) {
                    CreditCardModel cart =  snapshot.data[index];
                    return  InkWell(
                        onTap: (){},
                        child: Container(
                          decoration: BoxDecoration(
                              border: Border(bottom: BorderSide(width: 1, color: Colors.black12))
                          ),
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                child: Icon(Icons.credit_card, size: 50, color: Colors.blueGrey,),
                                flex: 1,
                              ),
                              Expanded(
                                child: Text("****" + cart.cardNumber.substring(0, 4),style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black54), ),
                                flex: 5,
                              )
                            ],
                          ),
                        ),
                    );
                  }),
            );
          }else if(snapshot.hasError) {
            return new Text('Não foi possível listar as formas de pagamentos ' +snapshot.error.toString(), style: TextStyle(color: Colors.red[200]),);
          }
          else{
            return Center(child: CircularProgressIndicator());
          }

        }
    ): Text("Carregando usuário....");
  }
}