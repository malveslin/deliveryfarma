
import 'package:flutter/material.dart';

class FarmaciaCard extends StatelessWidget {
  final String image, title;
  final Color backgroundColor;
  final GestureTapCallback MonTap;


  const FarmaciaCard({
    Key key,
    @required String this.image,
    @required String this.title,
    Color this.backgroundColor,
    this.MonTap
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: MonTap,
      child: Container(
        decoration: BoxDecoration(
          color: backgroundColor,
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
            boxShadow: [
              BoxShadow(blurRadius: 5.0,color: Colors.grey.shade200,spreadRadius: 2.0)
            ]
        ),
        margin: EdgeInsets.symmetric(vertical: 2.0),
        padding: EdgeInsets.all(5.0),
        width: 128,
        child: Column(
          mainAxisAlignment:MainAxisAlignment.center,
          children: <Widget>[
            Image.network(image,width: 100, height: 100,),
            Text(title, textAlign: TextAlign.center, style: TextStyle(fontStyle: FontStyle.italic,),)
          ],
        ),
      ),
    );
  }
}