
import 'package:flutter/material.dart';

import 'gtypography.dart';

class GroceryListItemTwo extends StatelessWidget {
  const GroceryListItemTwo({
    Key key,
    @required this.title,
    @required this.subtitle,
    @required this.image,
  }) : super(key: key);

  final String title;
  final String subtitle;
  final String image;

  @override
  Widget build(BuildContext context) {
    return Card(
        child: ListTile(
          leading: Container(
            height: 80.0,
            child: Image.network(image, height: 80.0,)),
          title: Container(
            height: 100.0,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new DTitle(text: title),
                new GrocerySubtitle(text: subtitle.toString())
              ],
            ),
          ),
          trailing: Column(
            children: <Widget>[
              IconButton(icon: Icon(Icons.add_shopping_cart,), color: Colors.green, onPressed: (){},)
            ],
          ),
        ),
      );
  }
}