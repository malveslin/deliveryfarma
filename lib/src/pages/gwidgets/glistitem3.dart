import 'package:deliveryfarma/models/produto.dart';
import 'package:flutter/material.dart';

import 'gtypography.dart';

class ListaProdutoCesta extends StatefulWidget {
  const ListaProdutoCesta(
      {Key key,
        @required this.produto})
      : super(key: key);

  final Produto produto;


  @override
  _ListaProdutoCestaState createState() => _ListaProdutoCestaState();
}

class _ListaProdutoCestaState extends State<ListaProdutoCesta> {
  Produto _produto;

  @override
  void initState() {
   setState(() {
     _produto = widget.produto;
   });
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Card(
        child: Row(
      children: <Widget>[
        Expanded(
          child: Container(
              height: 140.0,
              child: Image.network(
                _produto.imagem,
                height: 80.0,
              )),
          flex: 1,
        ),
        Expanded(
          child: Container(
            height: 140.0,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new DTitle(text: _produto.nome),
                new GrocerySubtitle(text: "R\$ " + _produto.preco.toString())
              ],
            ),
          ),
          flex: 2,
        ),
        Expanded(
          child: Column(
            children: <Widget>[
              IconButton(
                  icon: Icon(
                    Icons.add_circle,
                  ),
                  color: Colors.blue,
                  onPressed: () {}),
              Text(
                _produto.qtdPedido.toString(),
                textAlign: TextAlign.right,
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              IconButton(
                  icon: Icon(
                    Icons.remove_circle,
                  ),
                  color: Colors.red,
                  onPressed: () {})
            ],
          ),
          flex: 1,
        ),
      ],
    ));
  }
}
