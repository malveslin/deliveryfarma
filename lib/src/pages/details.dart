import 'dart:convert';

import 'package:deliveryfarma/business/produtodata.dart';
import 'package:deliveryfarma/models/produto.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'gwidgets/glistitem2.dart';
import 'gwidgets/gtypography.dart';
import 'home.dart';

class DetailsPage extends StatefulWidget {
  final Produto produto;

  DetailsPage(this.produto);

  @override
  _DetailsPageState createState() => _DetailsPageState();
}

class _DetailsPageState extends State<DetailsPage> {
  static final String path = "lib/src/pages/grocery/gdetails.dart";

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();


  _adicionarCarrinho(Produto produto) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String cart = prefs.getString('carrinho');
    String farmacia = prefs.getString('farmacia');
    print(cart);
    if (cart == null) {
      prefs.setString('carrinho', json.encode({}));
    }

    cart = prefs.getString('carrinho');
    Map<dynamic, dynamic> carrinho = json.decode(cart);

    if (carrinho.isEmpty){
      prefs.setString('farmacia', produto.farmacia.documentID);
      farmacia = prefs.getString('farmacia');
    }


    if (farmacia != produto.farmacia.documentID){
      final snackBar = SnackBar(
        content: Text('Você não pode adicionar um produto de outra farmácia'),
        backgroundColor: Colors.red[400],
        action: SnackBarAction(
          label: '',
          onPressed: () {
            // Some code to undo the change.
          },
        ),
      );
      _scaffoldKey.currentState.showSnackBar(snackBar);
      return;
    }

    carrinho[produto.id] =
        carrinho.containsKey(produto.id) ? carrinho[produto.id] + 1 : 1;
    print(carrinho[produto.id]);
    print(carrinho);
    await prefs.setString('carrinho', json.encode(carrinho));
    final snackBar = SnackBar(
      content: Text('Produto adicionado na cesta com sucesso'),
      backgroundColor: Colors.green[400],
      duration: Duration(seconds: 5),
      action: SnackBarAction(
        label: '',
        onPressed: () {
          // Some code to undo the change.
        },
      ),
    );
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        centerTitle: true,
        elevation: 0,
        backgroundColor: Colors.blue[400],
        title: Text("Detalhes do produto"),
      ),
      body: _buildPageContent(context, this.widget.produto),
    );
  }

  Widget _buildPageContent(context, Produto produto) {
    return Column(
      children: <Widget>[
        Expanded(
          child: ListView(
            children: <Widget>[
              _buildItemCard(context, produto),
              Container(
                  padding: EdgeInsets.all(30.0),
                  child: GrocerySubtitle(
                      text: produto.descricao ?? "Descrição do produto")),
              Container(
                  padding: EdgeInsets.only(left: 20.0, bottom: 10.0),
                  child: DTitle(text: "Produtos relacionados")),
              Column(
                children: <Widget>[
                  StreamBuilder<Object>(
                      stream: ProdutoData.getProdutos(produto.farmacia),
                      builder: (context, AsyncSnapshot<dynamic> snapshot) {
                        if (snapshot.hasData) {
                          return Container(
                            height: MediaQuery.of(context).size.height,
                            width: MediaQuery.of(context).size.width,
                            child: new ListView.builder(
                                physics: NeverScrollableScrollPhysics(),
                                shrinkWrap: true,
                                itemCount: snapshot.data.length,
                                padding: const EdgeInsets.all(1.0),
                                itemBuilder: (context, index) {
                                  Produto prod = snapshot.data[index];
                                  if (produto.id == prod.id) return Text("");
                                  return InkWell(
                                      onTap: () => Navigator.pushReplacement(
                                          context,
                                          MaterialPageRoute(
                                              builder: (BuildContext context) =>
                                                  DetailsPage(prod))),
                                      child: GroceryListItemTwo(
                                          title: prod.nome,
                                          image: prod.imagem,
                                          subtitle: prod.preco.toString()));
                                }),
                          );
                        } else if (snapshot.hasError) {
                          return new Text(
                            'Não foi possível listar os prosutos' +
                                snapshot.error.toString(),
                            style: TextStyle(color: Colors.red[200]),
                          );
                        } else {
                          return Center(child: CircularProgressIndicator());
                        }
                      })
                ],
              ),
            ],
          ),
        ),
        Row(
          children: <Widget>[
            Expanded(
              child: Container(
                  color: Colors.green,
                  child: Row(children: <Widget>[
                    Expanded(
                      child: Icon(Icons.add, color: Colors.white,),
                      flex: 1,
                    ),
                    Expanded(
                      child: FlatButton(
                        color: Colors.green,
                        onPressed: () {
                          _adicionarCarrinho(produto);
                        },
                        child: Text(
                          "Adicionar na cesta",
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                      flex: 2,
                    )
                  ])),
            ),
            Expanded(
              child: Container(
                color: Colors.blue,
                child: FlatButton(
                  color: Colors.blue,
                  onPressed: () {
                    Navigator.pushReplacement(context, MaterialPageRoute(
                        builder: (BuildContext context) => HomePage(0)
                    ));
                  },
                  child: Text(
                    "Ir para cesta",
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ),
            )
          ],
        )
      ],
    );
  }

  Widget _buildItemCard(context, Produto produto) {
    return Stack(
      children: <Widget>[
        Card(
          margin: EdgeInsets.only(top: 20.0, left: 20.0, right: 20.0),
          child: Container(
            padding: EdgeInsets.all(20.0),
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    IconButton(
                      onPressed: () {},
                      icon: Icon(Icons.favorite_border),
                    )
                  ],
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  child: Image.network(
                    produto.imagem,
                    height: 200,
                  ),
                ),
                SizedBox(
                  height: 10.0,
                ),
                DTitle(text: produto.nome),
                SizedBox(
                  height: 5.0,
                ),
                GrocerySubtitle(text: produto.preco.toStringAsFixed(2))
              ],
            ),
          ),
        ),
      ],
    );
  }
}
