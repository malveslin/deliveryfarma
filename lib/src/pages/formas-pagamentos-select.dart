import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:deliveryfarma/business/cielo.dart';
import 'package:deliveryfarma/business/pedidodata.dart';
import 'package:deliveryfarma/models/cesta.dart';
import 'package:deliveryfarma/models/creditcard.dart';
import 'package:deliveryfarma/models/pedido.dart';
import 'package:deliveryfarma/models/produto.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cielo/flutter_cielo.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'add-forma-pagamento.dart';

class FormaPagamentoPageSelect extends StatefulWidget {
  final Cesta cesta;
  FormaPagamentoPageSelect(this.cesta);

  @override
  _FormaPagamentoPageSelectState createState() => _FormaPagamentoPageSelectState();

}

class _FormaPagamentoPageSelectState extends State<FormaPagamentoPageSelect> {


  final FirebaseAuth firebaseAuth = FirebaseAuth.instance;
  var currentUser = null;
  int _codigoPedido = 0;
 Color  _colorIcon =  Colors.blueGrey;

  _limparCesta() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove("farmacia");
    prefs.remove("carrinho");
  }

  @override
  Future initState()  {
    super.initState();

    firebaseAuth.onAuthStateChanged
        .firstWhere((user) => user != null)
        .then((user) {
      setState(() {
        currentUser =  user;
      });
    });

    PedidoData.getCodigoPedido().then((valor) {
     setState(() {
       _codigoPedido =  valor;
     });
    });
  }



  @override
  Widget build(BuildContext context) {
      return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          elevation: 0,
          backgroundColor: Colors.blue[400],
          title: Text("Pagamento"),
        ),
        body: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: FutureBuilder(
                builder: (context, snapshot) {
                  return Text("Selecione uma forma de Pagamento");
                }
              ),
            ),
            Container(child: _buildPageContent(context, widget.cesta)),
          ],
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.push(context, MaterialPageRoute(
                builder: (BuildContext context) => FormaPagamentoAddPage()
            ));
          },
            backgroundColor: Colors.blue[900],
          child: Icon(Icons.add,)),
      );
    }

  Widget _buildPageContent(context, Cesta cesta) {
    return currentUser != null ? StreamBuilder<Object>(
        stream: Cielo.getFormasPagamento(currentUser.uid),
        builder: (context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.hasData) {
            return Container(
              height: MediaQuery.of(context).size.height - 200,
              width: MediaQuery.of(context).size.width,
              child: new ListView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: snapshot.data.length,
                  padding: const EdgeInsets.all(1.0),
                  itemBuilder: (context, index) {
                    CreditCardModel cart =  snapshot.data[index];

                    DocumentReference userRef = Firestore.instance.collection("users").document(currentUser.uid);
                    DocumentReference farmaciaRef =  widget.cesta.farmacia;
                    Pedido pedido = new  Pedido(
                        user: userRef,
                        farmacia: farmaciaRef,
                        codigo:  _codigoPedido,
                        descricao: "Pedido " + _codigoPedido.toString(),
                        data: Timestamp.now(),
                        response: {}
                    );


                    Sale sale = Sale(
                        merchantOrderId: _codigoPedido.toString(), // id único de sua venda
                        customer: Customer( //objeto de dados do usuário
                            name: currentUser.displayName.toString()
                        ),
                        payment: Payment(    // objeto para de pagamento
                            type: TypePayment.creditCard, //tipo de pagamento
                            amount: cesta.valorEmCentavos(), // valor da compra em centavos
                            installments: 1, // número de parcelas
                            softDescriptor: "DFarm"+_codigoPedido.toString(), //descrição que aparecerá no extrato do usuário. Apenas 13 caracteres
                            creditCard: CreditCard( //objeto de Cartão de crédito
                              cardNumber: cart.cardNumber, //número do cartão
                              holder: cart.cardHolderName, //nome do usuário impresso no cartão
                              expirationDate: cart.expiryDate, // data de expiração
                              securityCode: cart.cvvCode, // código de segurança
                              brand: cart.brand, // bandeira
                            )
                        )
                    );

                    return  InkWell(
                        onTap: () async {
                          final successSnackBar = SnackBar(
                            content: Text("Processando pagamento"),
                            backgroundColor:Colors.green[400],
                          );

                          final finalSnackBar = SnackBar(
                            content: Text("Pagamento efetuado com sucesso pagamento!"),
                            backgroundColor:Colors.green[400],
                          );

                          final errorSnackBar = SnackBar(
                            content: Text("Erro ao efetuar pagamento"),
                            backgroundColor:Colors.red[400],
                          );

                          try{
                            Scaffold.of(context).showSnackBar(successSnackBar);
                            var response = await Cielo.cielo.createSale(sale);
                            await PedidoData.savePedido(pedido).then((f){
                              cesta.produtos.forEach((produto){
                                DocumentReference prodref= Firestore.instance.collection("produtos").document(produto.id);
                                Firestore.instance.collection("pedidos").document(pedido.codigo.toString()).collection("produtos").document(produto.id).setData({"produto": prodref});
                              });
                              Firestore.instance.collection("pedidos").document(pedido.codigo.toString()).updateData({"response": response.toJson()});
                            });

                            print(response.payment.paymentId);
                            Scaffold.of(context).showSnackBar(finalSnackBar);
                            _limparCesta();
                          } catch(e){
                            print(e.message);
                            print(e.errors[0].message);
                            print(e.errors[0].code);
                            Scaffold.of(context).showSnackBar(errorSnackBar);
                          }

                        },
                        child: Container(
                          decoration: BoxDecoration(
                              border: Border(bottom: BorderSide(width: 1, color: Colors.black12))
                          ),
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                child: Icon(Icons.credit_card, size: 50, color: Colors.blueGrey,),
                                flex: 1,
                              ),
                              Expanded(
                                child: Text(cart.cardNumber.substring(0, 4) + " ****  **** " + cart.cardNumber.substring(15, 19),style: TextStyle(fontWeight: FontWeight.w600, color: Colors.black54), ),
                                flex: 5,
                              ),
                              Expanded(
                                child: Icon(Icons.check_box, size: 20, color: _colorIcon,),
                                flex: 1,
                              )

                            ],
                          ),
                        ),
                    );
                  }),
            );
          }else if(snapshot.hasError) {
            return new Text('Não foi possível listar as formas de pagamentos ' +snapshot.error.toString(), style: TextStyle(color: Colors.red[200]),);
          }
          else{
            return Center(child: CircularProgressIndicator());
          }

        }
    ): Text("Carregando usuário....");
  }
}