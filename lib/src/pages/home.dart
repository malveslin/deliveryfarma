
  
import 'package:deliveryfarma/src/pages/tabs/baskettab.dart';
import 'package:deliveryfarma/src/pages/tabs/hometab.dart';
import 'package:deliveryfarma/src/pages/tabs/remediotab.dart';
import 'package:flutter/material.dart';
import 'package:deliveryfarma/src/pages/tabs/hometab.dart';
import 'package:deliveryfarma/src/pages/tabs/profiletab.dart';
import 'package:deliveryfarma/src/pages/tabs/favoritetab.dart';
import 'package:deliveryfarma/src/pages/tabs/maptab.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:deliveryfarma/models/farmacia.dart';
import 'package:deliveryfarma/business/farmdata.dart';

class HomePage extends StatefulWidget {
  static final String path = "lib/src/pages/grocery/ghome.dart";
  final int index;
  HomePage(this.index);

  @override
  HomePageState createState() {
    return new HomePageState();
  }
}

class HomePageState extends State<HomePage> {
  int _currentIndex = 0;
  List<Widget> _children = [];
  List<Widget> _appBars = [];

  @override
  void initState() {
    _children.add(MapTabView(""));
    _children.add(BasketTabView());
    _children.add(FavoriteTabView());
    _children.add(ProfileTabView());

    _appBars.add(_buildAppBar());
    _appBars.add(_buildAppBarOne("Minha Cesta"));
    _appBars.add(_buildAppBarOne("Pedidos"));
    _appBars.add(_buildAppBarOne("Eu"));

    super.initState();
  }


  @override
    Widget build(BuildContext context) {
      return Scaffold(
        appBar: _appBars[_currentIndex],
        body: _children[_currentIndex],
        bottomNavigationBar: _buildBottomNavigationBar(),
      );
    }

  Widget _buildAppBar() {
    return PreferredSize(
        preferredSize: Size.fromHeight(90.0),
        child: Container(
          margin: EdgeInsets.only(top: 20.0),
          child: AppBar(
            elevation: 0,
            backgroundColor: Colors.transparent,
            title: Container(
              child: Card(
                child: Container(
                  child: TextField(
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 15.0),
                      hintText: "Procurar farmácia",
                      border: InputBorder.none,
                        suffixIcon: IconButton(onPressed: (){
                        }, icon: Icon(Icons.search))
                    ),
                    onTap: (){
                      showSearch(context: context, delegate: FarmaSearch());
                    },
                  ),
                ),
              ),
            ),
            leading: Image.asset('assets/img/rocket.png',scale: 0.3,),
          ),
        ),
      );
  }
  Widget _buildAppBarOne(String title) {
    return AppBar(
      bottom: PreferredSize(child: Container(color: Colors.grey.shade200, height: 1.0,), preferredSize: Size.fromHeight(1.0)),
      automaticallyImplyLeading: false,
      backgroundColor: Colors.white,
      elevation: 0,
      title: Text(title, style: TextStyle(color: Colors.black)),
    );
  }


  BottomNavigationBar _buildBottomNavigationBar() {
    return BottomNavigationBar(
      onTap: _onTabTapped,
      items: <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(Icons.location_on),
          title: Text("Início")),
        BottomNavigationBarItem(
          icon: Icon(Icons.shopping_basket),
          title: Text("Cesta")),
        BottomNavigationBarItem(
          icon: Icon(Icons.receipt),
          title: Text("Pedidos")),
        BottomNavigationBarItem(
          icon: Icon(Icons.person_outline),
          title: Text("Eu")),
      ],
      type: BottomNavigationBarType.fixed,
      currentIndex: _currentIndex,
    );
  }

  _onTabTapped(int index) {
    setState(() {
          _currentIndex = index;
        });
  }

}



class FarmaSearch extends SearchDelegate<String>{
  List<Farmacia> farmacias;
  List<String> farmaNames = [];
  List<String> sugestFarmacias = [];

  FarmaSearch() {
    _setFarmacias();
    FarmData.setFarmas();
  }

  _setFarmacias() async{
    this.farmacias = await FarmData.getFarmas();
    this.farmaNames = farmacias.map((f) => f.nome).toList();
    this.sugestFarmacias.add(farmaNames[0]);
  }


  @override
  List<Widget> buildActions(BuildContext context) {
    // TODO: implement buildActions
    return [IconButton(
      icon: Icon(Icons.clear),
      onPressed: (){
        query = "";
      },)];
  }




  @override
  Widget buildLeading(BuildContext context) {
    // TODO: implement buildLeading
    return IconButton(
      icon: AnimatedIcon(
          icon: AnimatedIcons.menu_arrow,
          progress: transitionAnimation
      ),
      onPressed: (){
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    // TODO: implement buildResults
    return MapTabView(query);
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    final suggestionList = query.isEmpty? sugestFarmacias : farmaNames.where((p) => p.contains(query)).toList();

    return StreamBuilder(
      stream: FarmData.getFarmacias(),
      builder: (context, AsyncSnapshot<dynamic> snapshot){
        if(snapshot.hasData){
          return ListView.builder(
            itemBuilder: (context, index) => ListTile(
              onTap: (){
                query = suggestionList[index];
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => MapTabView(query)),
                );
              },
              leading: Icon(Icons.local_hospital),
              title: Text(suggestionList[index]),
            ),
            itemCount: suggestionList.length,
          );
        }
      },
    );
  }


}




