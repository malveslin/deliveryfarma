import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class ProfilePage extends StatefulWidget {
  ProfilePage();

  @override
  _ProfilePageState createState() => _ProfilePageState();

}

class _ProfilePageState extends State<ProfilePage> {


  final FirebaseAuth firebaseAuth = FirebaseAuth.instance;
  FirebaseUser currentUser = null;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    firebaseAuth.onAuthStateChanged
        .firstWhere((user) => user != null)
        .then((user) {
      setState(() {
        currentUser =  user;
      });
    });
  }

  @override
    Widget build(BuildContext context) {
      return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          elevation: 0,
          backgroundColor: Colors.blue[400],
          title: Text("Minha Conta"),
        ),
        body: currentUser != null ?  Column(
          children: <Widget>[
            Container(
              decoration: new BoxDecoration(
                color: Colors.purple,
                gradient: new LinearGradient(
                    colors: [Colors.blue[400], Colors.cyan],
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter
                ),
              ),
              child: Center(
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: CircleAvatar(
                        radius: 48.0,
                        backgroundImage: NetworkImage(currentUser.photoUrl),
                      ),
                    ),
                    Text(currentUser.displayName, style: TextStyle(color: Colors.white),),
                    Text(currentUser.email, style: TextStyle(color: Colors.white),),
                    SizedBox(height: 10,)
                  ],
                ),
              ),
            )
          ],
        ): Center(
          child: CircularProgressIndicator(),
        )
      );
    }

}