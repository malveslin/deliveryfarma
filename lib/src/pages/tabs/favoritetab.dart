import 'package:date_format/date_format.dart';
import 'package:deliveryfarma/business/pedidodata.dart';
import 'package:deliveryfarma/models/pedido.dart';
import 'package:deliveryfarma/src/pages/gwidgets/glistitem3.dart';
import 'package:flutter/material.dart';

class FavoriteTabView extends StatelessWidget {
  @override
    Widget build(BuildContext context) {
      return Column(

        children: <Widget>[
          Expanded(
            child: ListView(
              padding: EdgeInsets.all(5.0),
              children: <Widget>[
                FutureBuilder<Object>(
                    future: PedidoData.getPedidoByUserFuture(),
                    builder: (context, AsyncSnapshot<dynamic> snapshot) {
                      if (snapshot.hasData) {
                        return Container(
                          height: MediaQuery.of(context).size.height,
                          width: MediaQuery.of(context).size.width,
                          child: new ListView.builder(
                              physics: NeverScrollableScrollPhysics(),
                              shrinkWrap: true,
                              itemCount: snapshot.data.length,
                              padding: const EdgeInsets.all(1.0),
                              itemBuilder: (context, index) {
                                Pedido pedido = snapshot.data[index];
                                return  _pedidoCard(context, pedido);
                              }),
                        );
                      }else if(snapshot.hasError) {
                        return new Text('Não foi possível listar os produtos' +snapshot.error.toString(), style: TextStyle(color: Colors.red[200]),);
                      }
                      else{
                        return Center(child: CircularProgressIndicator());
                      }

                    }
                )
              ],
            ),
          ),
          SizedBox(height: 5.0,),
          _buildTotals()
        ],
      );
    }

  Widget _buildTotals() {
    return Container(
      padding: EdgeInsets.only(left: 20.0, right: 20.0),
      child: RaisedButton(
        color: Colors.green,
        onPressed: (){},
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Text("Adicionar a cesta", style: TextStyle(color: Colors.white)),
          ],
        ),
      )
    );
  }

  Widget _pedidoCard(BuildContext context, Pedido pedido ) {
    return Card(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Expanded(
              child:Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(pedido.descricao, style: TextStyle(fontWeight: FontWeight.w600, fontSize: 16), ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(formatDate(pedido.data.toDate(), [dd, '/', mm, '/', yyyy, ' ',  HH, ':', nn]), style: TextStyle(fontStyle: FontStyle.italic, fontSize: 12), ),
                  ),
                ],
              ),
              flex: 1,
            ),
            Expanded(
              child:Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(0.0),
                    child:  IconButton(icon: Icon(Icons.print), onPressed: null)
                  ),
                  Padding(
                    padding: const EdgeInsets.all(0.0),
                    child: IconButton(icon: Icon(Icons.refresh, color: Colors.green[200],), onPressed: null)
                  ),
                ],
              ),
              flex: 1,
            ),
          ],
        ));
  }
}