import 'package:deliveryfarma/src/pages/gwidgets/gtypography.dart';
import 'package:flutter/material.dart';

import '../formas-pagamentos.dart';
import '../login.dart';
import '../profile.dart';

class ProfileTabView extends StatelessWidget {
  @override
    Widget build(BuildContext context) {
      return Container(
        color: Colors.white,
        child: ListView(
          padding: EdgeInsets.all(10.0),
          children: <Widget>[
            ListTile(
              onTap: (){},
              leading: Icon(Icons.edit),
              title: DTitle(text: "Minhas compras"),
            ),
            ListTile(
              onTap: (){},
              leading: Icon(Icons.favorite_border),
              title: DTitle(text: "Meus Favoritos"),
            ),
            ListTile(
              onTap: (){
                Navigator.push(context, MaterialPageRoute(
                    builder: (BuildContext context) => FormaPagamentoPage()
                ));
              },
              leading: Icon(Icons.credit_card),
              title: DTitle(text: "Formas de Pagamento"),
            ),
            ListTile(
              onTap: (){
                Navigator.push(context, MaterialPageRoute(
                    builder: (BuildContext context) => ProfilePage()
                ));
              },
              leading: Icon(Icons.settings),
              title: DTitle(text: "Minha Conta"),
            ),
            ListTile(
              onTap: (){
                Navigator.push(context, MaterialPageRoute(
                    builder: (BuildContext context) => Login()
                ));
              },
              leading: Icon(Icons.exit_to_app),
              title: DTitle(text: "Sair"),
            ),
          ],
        ),
      );
    }
}