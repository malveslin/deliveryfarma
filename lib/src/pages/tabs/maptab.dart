import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import 'package:deliveryfarma/business/farmdata.dart';
import '../produtos.dart';
import 'package:location/location.dart';

import 'package:flutter/services.dart';

import 'package:deliveryfarma/models/farmacia.dart';

class MapTabView extends StatefulWidget {
  String name;

  MapTabView(this.name);

  @override
  _MapTabViewState createState() => _MapTabViewState(this.name);
}

class _MapTabViewState extends State<MapTabView> {
  var pos;
  BitmapDescriptor myIcon;
  String _mapStyle;
  GoogleMapController mapController;
  String farmaName;
  double la, lo;
  List<Farmacia> farmacias = [];

  _MapTabViewState(this.farmaName);

  @override
  void initState() {

    this.farmacias = FarmData.farmacias;
    rootBundle.loadString('assets/map_style.txt').then((string) {
      _mapStyle = string;
    });

    if (!farmaName.isEmpty) {
      farmacias.forEach((f) {
        if (farmaName == f.nome) {
          pos = {
            'latitude': f.geolocation.latitude,
            'longitude': f.geolocation.longitude,
          };
        }
      });
    } else {
      _getStartLocation();
    }

    BitmapDescriptor.fromAssetImage(
            ImageConfiguration(size: Size(5, 5)), 'assets/img/maker.png')
        .then((onValue) {
      myIcon = onValue;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return _buildMap();
  }

  Widget _buildMap() {
    return StreamBuilder<QuerySnapshot>(
        stream: Firestore.instance.collection("farmacia").snapshots(),
        builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasData) {
            return new Scaffold(
              body: GoogleMap(
                  mapType: MapType.normal,
                  initialCameraPosition: setStartPosition(),
                  markers: Set.from(getMarkers(snapshot)),
                  myLocationEnabled: true,
                  onMapCreated: (GoogleMapController controller) {
                    mapController = controller;
                    mapController.setMapStyle(_mapStyle);
                  }),
            );
          } else {
            return Center(child: CircularProgressIndicator());
          }
        });
  }

  _getStartLocation() async {
    try {
      pos = await (new Location()).getLocation();
    } on PlatformException {
      pos = null;
    }
  }

  void _openListProdutos(BuildContext context, String idFarmacia) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (BuildContext context) =>
                ProdutosPage(farmaciaRef: FarmData.getReference(idFarmacia))));
  }

  getMarkers(AsyncSnapshot<QuerySnapshot> snapshot) {
    return snapshot.data.documents
        .map((doc) => new Marker(
            markerId: MarkerId(doc["nome"]),
            icon: myIcon,
            draggable: false,
            onTap: () {
              _openListProdutos(context, doc.documentID);
            },
            infoWindow: InfoWindow(title: doc["nome"], snippet: ""),
            position: LatLng(
                doc["geolocation"].latitude, doc["geolocation"].longitude)))
        .toList();
  }

  setStartPosition() {
    return CameraPosition(
      target: LatLng(pos["latitude"], pos["longitude"]),
      zoom: 16.31,
    );
  }

  setSearchPosition(double la, double lo) {
    mapController.moveCamera(CameraUpdate.newCameraPosition(CameraPosition(
      target: LatLng(la, lo),
      zoom: 16.31,
    )));
  }

}
