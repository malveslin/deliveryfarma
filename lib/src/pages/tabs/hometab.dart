
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:deliveryfarma/business/farmdata.dart';
import 'package:deliveryfarma/models/farmacia.dart';
import 'package:deliveryfarma/src/pages/details.dart';
import 'package:deliveryfarma/src/pages/gwidgets/farmcard.dart';
import 'package:deliveryfarma/src/pages/gwidgets/listitem1.dart';
import 'package:deliveryfarma/src/pages/gwidgets/glistitem2.dart';
import 'package:deliveryfarma/src/pages/tabs/remediotab.dart';
import 'package:flutter/material.dart';

import '../produtos.dart';

class HomeTabView extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        _buildFarmacias()
      ],
    );
  }


  void _openListProdutos(BuildContext context, String idFarmacia) {

    Navigator.push(context, MaterialPageRoute(
        builder: (BuildContext context) => ProdutosPage(farmaciaRef: FarmData.getReference(idFarmacia))
    ));
  }


  Widget _buildListHeader(String left, String right) {
    return Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(left: 10),
              padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
              color: Colors.red,
              child: Text(left,
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(right: 10.0),
              padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
              child: InkWell(
                onTap: (){},
                child: Text(right,
                  style: TextStyle(
                    color: Colors.red,
                  ),
                ),
              ),
            ),
          ],
        )
      );
  }

  Widget _buildFarmacias() {
    return StreamBuilder<Object>(
      stream: FarmData.getFarmacias(),
      builder: (context, AsyncSnapshot<dynamic> snapshot) {
        if (snapshot.hasData) {
          return Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: new GridView.builder(
            shrinkWrap: true,
            itemCount: snapshot.data.length,
                gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
            padding: const EdgeInsets.all(30.0),
            itemBuilder: (context, index) {
              Farmacia farmacia =  snapshot.data[index];
              return Row(
              children: <Widget>[
                FarmaciaCard(
                  image: farmacia.foto,
                  title: farmacia.nome,
                  backgroundColor: Colors.white,
                    MonTap: () {
                      _openListProdutos(context, farmacia.id);
                    },
                )
              ],
              );
            }),
          );
        } else{
          return Center(child: CircularProgressIndicator());
        }
      }
    );
  }

}