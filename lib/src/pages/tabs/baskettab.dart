import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:deliveryfarma/business/produtodata.dart';
import 'package:deliveryfarma/models/cesta.dart';
import 'package:deliveryfarma/models/produto.dart';
import 'package:deliveryfarma/src/pages/formas-pagamentos-select.dart';
import 'package:deliveryfarma/src/pages/gwidgets/glistitem3.dart';
import 'package:deliveryfarma/src/pages/gwidgets/gtypography.dart';

  
import 'package:flutter/material.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:shared_preferences/shared_preferences.dart';


class BasketTabView extends StatefulWidget {
  @override
  _BasketTabViewState createState() => _BasketTabViewState();
}

class _BasketTabViewState extends State<BasketTabView> {


  Cesta _cesta = new Cesta();

  _getCarrinho () async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String cart  =  prefs.getString('carrinho');
    String farm  =  prefs.getString('farmacia');
    if (cart == null) {
      prefs.setString('carrinho', json.encode({}));
    }
    cart  =  prefs.getString('carrinho');
    Map<dynamic, dynamic> carrinho = json.decode(cart);

    List<Produto> produtos = [];
     await Future.forEach (carrinho.entries.toList(), (doc) async{
       var qtd = doc.value;
       await Firestore.instance
           .collection("produtos").document(doc.key).get().then((doc) {
            Produto produto = Produto.fromDocument(doc);
            produto.qtdPedido = qtd;
            produtos.add(produto);
       });
    });
     if (this.mounted) {
       setState(() {
         _cesta.produtos = produtos;
         _cesta.farmacia =  Firestore.instance.collection("farmacia").document(farm);
       });
     }

    return produtos;
  }

  _decrementarAumentar(produto, op) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String cart  =  prefs.getString('carrinho');
    if (cart == null) {
      prefs.setString('carrinho', json.encode({}));
    }
    cart  =  prefs.getString('carrinho');
    Map<dynamic, dynamic> carrinho = json.decode(cart);
    if (op == "+"){
      carrinho[produto.id] = carrinho[produto.id] + 1;
    }else{
      if (carrinho[produto.id] <= 1) {
        carrinho.remove(produto.id);
      }else{
        carrinho[produto.id] = carrinho[produto.id] - 1;
      }
    }
    await prefs.setString('carrinho', json.encode(carrinho));
    _getCarrinho();
  }

  @override
    Widget build(BuildContext context) {
      return Column(
        children: <Widget>[
          Expanded(
            child: ListView(
              padding: EdgeInsets.all(10.0),
              children: <Widget>[
                FutureBuilder<Object>(
                    future: _getCarrinho(),
                    builder: (context, AsyncSnapshot<dynamic> snapshot) {
                      if (snapshot.hasData) {
                        return Container(
                          height: MediaQuery.of(context).size.height,
                          width: MediaQuery.of(context).size.width,
                          child: new ListView.builder(
                              physics: NeverScrollableScrollPhysics(),
                              shrinkWrap: true,
                              itemCount: snapshot.data.length,
                              padding: const EdgeInsets.all(1.0),
                              itemBuilder: (context, index) {
                                Produto produto = snapshot.data[index];
                               return   _cardCesta(context, produto);
                              }),
                        );
                      }else if(snapshot.hasError) {
                        return new Text('Não foi possível listar os produtos' +snapshot.error.toString(), style: TextStyle(color: Colors.red[200]),);
                      }
                      else{
                        return Center(child: CircularProgressIndicator());
                      }

                    }
                )
              ],
            ),
          ),
          SizedBox(height: 10.0,),
          _buildTotals()
        ],
      );
    }

  Widget _buildTotals() {
    return ClipOval(
      clipper: OvalTopBorderClipper(),
      child: Container(
            height: 180,
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(blurRadius: 5.0,color: Colors.grey.shade700,spreadRadius: 80.0),
              ],
              color: Colors.white,
            ),
            padding: EdgeInsets.only(left: 20.0, right: 20.0, top: 40.0, bottom: 10.0),
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text("Subtotal"),
                    _cesta.produtos.isNotEmpty ? Text("R\$ " + _cesta.subtotal().toStringAsFixed(2)) : Text(""),
                  ],
                ),
                SizedBox(height: 10.0,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text("Frete"),
                    _cesta.produtos.isNotEmpty ? Text("R\$ "+ _cesta.frete.toStringAsPrecision(2)) : Text(""),
                  ],
                ),
                SizedBox(height: 10.0,),  
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text("Total"),
                    _cesta.produtos.isNotEmpty ? Text("R\$ " + (_cesta.frete + _cesta.subtotal()).toStringAsFixed(2)): Text(""),
                  ],
                ),
                SizedBox(height: 10.0,),
                RaisedButton(
                  color: _cesta.produtos.isEmpty ? Colors.black12 : Colors.green,
                  disabledElevation: 0,
                  onPressed: _cesta.produtos.isEmpty ? () {} : () {
                    Navigator.push(context, MaterialPageRoute(
                        builder: ( BuildContext context) => FormaPagamentoPageSelect(_cesta)
                    ));
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                        !_cesta.produtos.isEmpty ? Text("Continuar para pagamento", style: TextStyle(color: Colors.white)) : Text("Cesta vazia", style: TextStyle(color: Colors.white)) ,
                        !_cesta.produtos.isEmpty ? Text(" R\$ " + (_cesta.frete + _cesta.subtotal()).toStringAsFixed(2), style: TextStyle(color: Colors.white)) : Text(""),

                    ],
                  ),
                )
              ],
            ),
          ),
    );
  }




  Widget _cardCesta(BuildContext context, Produto produto ) {
     return Card(
        child: Row(
          children: <Widget>[
            Expanded(
              child: Container(
                  height: 140.0,
                  child: Image.network(
                    produto.imagem,
                    height: 80.0,
                  )),
              flex: 1,
            ),
            Expanded(
              child: Container(
                height: 140.0,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new DTitle(text: produto.nome),
                    new GrocerySubtitle(text: "R\$ " + produto.preco.toString())
                  ],
                ),
              ),
              flex: 2,
            ),
            Expanded(
              child: Column(
                children: <Widget>[
                  IconButton(
                      icon: Icon(
                        Icons.add_circle,
                      ),
                      color: Colors.blue,
                      onPressed: () {_decrementarAumentar(produto, "+");}),
                  Text(
                    produto.qtdPedido.toString(),
                    textAlign: TextAlign.right,
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  IconButton(
                      icon: Icon(
                        Icons.remove_circle,
                      ),
                      color: Colors.red,
                      onPressed: () {_decrementarAumentar(produto, "-");})
                ],
              ),
              flex: 1,
            ),
          ],
        ));
  }
}