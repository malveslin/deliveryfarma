
import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:deliveryfarma/business/farmdata.dart';
import 'package:deliveryfarma/business/produtodata.dart';
import 'package:deliveryfarma/models/farmacia.dart';
import 'package:deliveryfarma/models/produto.dart';
import 'package:deliveryfarma/src/pages/tabs/remediotab.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'details.dart';
import 'gwidgets/farmcard.dart';
import 'gwidgets/glistitem2.dart';
import 'gwidgets/listitem1.dart';

class ProdutosPage extends StatefulWidget {

  DocumentReference farmaciaRef;
  ProdutosPage({@required this.farmaciaRef});

  @override
  _ProdutosPageState createState() => _ProdutosPageState();
}

class _ProdutosPageState extends State<ProdutosPage> {
  static final String path = "lib/src/pages/grocery/gdetails.dart";
  String _query =  "";
  final TextEditingController _buscaController = new TextEditingController();

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();


  _adicionarCarrinho(Produto produto) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String cart = prefs.getString('carrinho');
    String farmacia = prefs.getString('farmacia');
    print(cart);
    if (cart == null) {
      prefs.setString('carrinho', json.encode({}));
    }

    cart = prefs.getString('carrinho');
    Map<dynamic, dynamic> carrinho = json.decode(cart);

    if (carrinho.isEmpty){
      prefs.setString('farmacia', produto.farmacia.documentID);
      farmacia = prefs.getString('farmacia');
    }


    if (farmacia != produto.farmacia.documentID){
      final snackBar = SnackBar(
        content: Text('Você não pode adicionar um produto de outra farmácia'),
        backgroundColor: Colors.red[400],
        action: SnackBarAction(
          label: '',
          onPressed: () {
            // Some code to undo the change.
          },
        ),
      );
      _scaffoldKey.currentState.showSnackBar(snackBar);
      return;
    }

    carrinho[produto.id] =
    carrinho.containsKey(produto.id) ? carrinho[produto.id] + 1 : 1;
    print(carrinho[produto.id]);
    print(carrinho);
    await prefs.setString('carrinho', json.encode(carrinho));
    final snackBar = SnackBar(
      content: Text('Produto adicionado na cesta com sucesso'),
      backgroundColor: Colors.green[400],
      action: SnackBarAction(
        label: '',
        onPressed: () {
          // Some code to undo the change.
        },
      ),
    );
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  _buscarProdutos(String nome){
      setState(() {
        this._query = nome;
      });
  }


  @override
    Widget build(BuildContext context) {
      return Scaffold(
        appBar: _buildAppBar(_buscaController, this),
        key: _scaffoldKey,
        body: ListView(
          children: <Widget>[
            _buildFarmacia(),
            SizedBox(height: 10.0,),
            _buildListHeader("POPULARES","MAIS PRODUTOS"),
            SizedBox(height: 10.0,),
            StreamBuilder<Object>(
              stream: ProdutoData.getProdutosSearch(widget.farmaciaRef,_query ),
              builder: (context, AsyncSnapshot<dynamic> snapshot) {
                print(snapshot.data);
                if (snapshot.hasData) {
                  return Container(
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    child: new ListView.builder(
                        shrinkWrap: true,
                        itemCount: snapshot.data.length,
                        padding: const EdgeInsets.all(1.0),
                        itemBuilder: (context, index) {
                          Produto produto =  snapshot.data[index];
                          return InkWell(
                          onTap: ()=> _openDetailPage(context, produto),
                          child: GroceryListItemTwo(title: produto.nome, image: produto.imagem, subtitle: produto.preco.toString(), ));
                        }),
                  );
                }else if(snapshot.hasError) {
                  return new Text('Não foi possível listar os prosutos' +snapshot.error.toString(), style: TextStyle(color: Colors.red[200]),);
                }
                else{
                  return Center(child: CircularProgressIndicator());
                }

              }
            ),
          ],
        ),

      );
    }

  void _openDetailPage(BuildContext context, Produto produto) {
    Navigator.push(context, MaterialPageRoute(
        builder: (BuildContext context) => DetailsPage(produto)
    ));
  }

  Widget _buildListHeader(String left, String right) {
    return Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(left: 10),
              padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
              color: Colors.red,
              child: Text(left,
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(right: 10.0),
              padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
              child: InkWell(
                onTap: (){},
                child: Text(right,
                  style: TextStyle(
                    color: Colors.red,
                  ),
                ),
              ),
            ),
          ],
        )
    );
  }

  Widget _buildFarmacia() {
    return FutureBuilder<Object>(
        future: FarmData.getFarmacia(widget.farmaciaRef),
        builder: (context, AsyncSnapshot snapshot) {
          if (snapshot.hasData) {
            Farmacia farmacia = snapshot.data;
            return Container(
              height: 100,
              width: MediaQuery.of(context).size.width,
              child: _headerFarm(farmacia.foto, farmacia.nome),
            );
          } else{
            return LinearProgressIndicator();
          }
        }
    );
  }

}

Widget _buildAppBar(TextEditingController controller, _ProdutosPageState state) {
  return PreferredSize(
    preferredSize: Size.fromHeight(90.0),
    child: Container(
      margin: EdgeInsets.only(top: 20.0),
      child: AppBar(
        elevation: 0,
        backgroundColor: Colors.blue[400],
        title: Container(
          child: Card(
            child: Container(
              child: TextField(
                decoration: InputDecoration(
                    contentPadding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 15.0),
                    hintText: "Procurar produto",
                    border: InputBorder.none,
                    suffixIcon: IconButton(onPressed: (){
                      state._buscarProdutos(controller.text);
                    }, icon: Icon(Icons.search))
                ),
                onChanged:  state._buscarProdutos(controller.text),
                controller: controller,
              ),
            ),
          ),
        ),
      ),
    ),
  );
}


Widget _headerFarm(String imagem, String nome){
  return Container(
    decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(8.0)),
        boxShadow: [
          BoxShadow(blurRadius: 10.0,color: Colors.grey.shade200,spreadRadius: 2.0)
        ]
    ),
    margin: EdgeInsets.symmetric(vertical: 2.0),
    padding: EdgeInsets.all(5.0),
    width: 128,
    child: Row(
      mainAxisAlignment:MainAxisAlignment.center,
      children: <Widget>[
        Expanded(child: Image.network(imagem,width: 100, height: 100,), flex: 1,),
        Expanded(child: Text(nome, textAlign: TextAlign.center, style: TextStyle(fontStyle: FontStyle.italic,),),flex: 2,)
      ],
    ),
  );
}