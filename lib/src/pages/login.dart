import 'package:deliveryfarma/business/auth.dart';
import 'package:deliveryfarma/business/validator.dart';
import 'package:deliveryfarma/src/pages/signup.dart';
import 'package:deliveryfarma/src/widgets/custom_alert_dialog.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'home.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  static final String path = "lib/src/pages/login/login2.dart";
  final TextEditingController _email = new TextEditingController();
  final TextEditingController _senha = new TextEditingController();
  final Firestore firestore = Firestore();

  void _emailLogin(
      {String email, String password, BuildContext context}) async {
    if (Validator.validateEmail(email) &&
        Validator.validatePassword(password)) {
      try {
        SystemChannels.textInput.invokeMethod('TextInput.hide');
        await Auth.signIn(email, password)
            .then((uid) =>  Navigator.pushReplacement(context, MaterialPageRoute(
            builder: (BuildContext context) => HomePage(0)
        )));
      } catch (e) {
        print("Error in email sign in: $e");
        String exception = Auth.getExceptionText(e);
        _showErrorAlert(
          title: "Login failed",
          content: exception,
          context: context,
          onPressed: () {},
        );
      }
    }
  }

  void _showErrorAlert({String title, String content, BuildContext context, VoidCallback onPressed}) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (context) {
        return CustomAlertDialog(
          content: content,
          title: title,
          onPressed: onPressed,
        );
      },
    );
  }

  checkLogin() async {
   var  user = await FirebaseAuth.instance.currentUser();
   print(user);
   if (user != null){
     Navigator.pushReplacement(context, MaterialPageRoute(
         builder: (BuildContext context) => HomePage(0)));
   }
  }

  Future _firestoreSetting() async{
    await firestore.settings(timestampsInSnapshotsEnabled: true);
  }

  initState(){
    checkLogin();
    _firestoreSetting();
    super.initState();

  }




  Widget _buildPageContent(BuildContext context) {
    return Container(
      color: Colors.blue.shade100,
      child: ListView(
        children: <Widget>[
          SizedBox(height: 30.0,),
          CircleAvatar(child: Image.asset('assets/img/rocket.png'), maxRadius: 50, backgroundColor: Colors.transparent,),
          SizedBox(height: 20.0,),
          _buildLoginForm(context),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              FlatButton(
                onPressed: (){
                  Navigator.pushReplacement(context, MaterialPageRoute(
                      builder: (BuildContext context) => SignupOnePage()
                  ));
                },
                child: Text("Criar conta", style: TextStyle(color: Colors.blue, fontSize: 18.0)),
              )
            ],
          )
        ],
      ),
    );
  }

  Container _buildLoginForm(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20.0),
      child: Stack(
        children: <Widget>[
          ClipPath(
            clipper: RoundedDiagonalPathClipper(),
            child: Container(
              height: 400,
              padding: EdgeInsets.all(10.0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(40.0)),
                color: Colors.white,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  _imput("Email", Icons.email, false, _email),
                  _imput("Senha", Icons.lock, true, _senha),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Container(padding: EdgeInsets.only(right: 20.0),
                          child: Text("Esqueci a senha",
                            style: TextStyle(color: Colors.black45),
                          )
                      )
                    ],
                  ),
                  SizedBox(height: 10.0,),

                ],
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              CircleAvatar(
                radius: 40.0,
                backgroundColor: Colors.blue.shade600,
                child: new Icon(FontAwesomeIcons.fileMedical, color: Colors.white,),
              ),
            ],
          ),
          Container(
            height: 420,
            child: Align(
              alignment: Alignment.bottomCenter,
              child: RaisedButton(
                onPressed: (){

                  _emailLogin(
                      email: _email.text,
                      password: _senha.text,
                      context: context);

                },
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(40.0)),
                child: Text("Login", style: TextStyle(color: Colors.white70)),
                color: Colors.blue,
              ),
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildPageContent(context),
    );
  }
}


Widget _imput(hint, IconData icon, bool password, TextEditingController controller) {
  return Column(
    children: <Widget>[
      Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          child: TextField(
            style: TextStyle(color: Colors.blue),
            obscureText: password,
            controller: controller ,
            decoration: InputDecoration(
                hintText: hint,
                hintStyle: TextStyle(color: Colors.blue.shade200),
                border: InputBorder.none,
                icon: Icon(icon, color: Colors.blue,)
            ),
          )
      ),
      Container(child: Divider(color: Colors.blue.shade400,), padding: EdgeInsets.only(left: 20.0,right: 20.0, bottom: 10.0),)

    ],
  );
}
