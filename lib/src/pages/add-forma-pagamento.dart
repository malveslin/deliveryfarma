import 'package:deliveryfarma/business/cielo.dart';
import 'package:deliveryfarma/models/creditcard.dart';
import 'package:deliveryfarma/src/widgets/cartaocredito.dart';
import 'package:deliveryfarma/src/widgets/cartaocreditoform.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cielo/flutter_cielo.dart';

class FormaPagamentoAddPage extends StatefulWidget {
  FormaPagamentoAddPage();

  @override
  _FormaPagamentoAddPageState createState() => _FormaPagamentoAddPageState();

}

class _FormaPagamentoAddPageState extends State<FormaPagamentoAddPage> {

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  String cardNumber = '';
  String expiryDate = '';
  String cardHolderName = '';
  String cvvCode = '';
  String brand = '';
  bool isCvvFocused = false;
  IconData _icon =  Icons.save;

  final FirebaseAuth firebaseAuth = FirebaseAuth.instance;
  var currentUser = null;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    firebaseAuth.onAuthStateChanged
        .firstWhere((user) => user != null)
        .then((user) {
          setState(() {
            currentUser =  user;
            cardHolderName =  user.displayName;
          });
    });
  }


  void onCreditCardModelChange(CreditCardModel creditCardModel) {
    setState(() {
      cardNumber = creditCardModel.cardNumber;
      expiryDate = creditCardModel.expiryDate;
      cardHolderName = creditCardModel.cardHolderName;
      cvvCode = creditCardModel.cvvCode;
      isCvvFocused = creditCardModel.isCvvFocused;
    });
  }


  @override
    Widget build(BuildContext context) {
      return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          centerTitle: true,
          elevation: 0,
          backgroundColor: Colors.blue[400],
          title: Text("Pagamento"),
        ),
        body: _buildPageContent(context, this),
        floatingActionButton: currentUser != null ? FloatingActionButton.extended(
          onPressed: () async {
            CreditCardModel cart = CreditCardModel(
              cardHolderName: cardHolderName,
              cardNumber: cardNumber,
              cvvCode: cvvCode,
              expiryDate: expiryDate,
              brand: CartaoCredito.getBrand(cardNumber),
              user: currentUser.uid
            );
            var salvou = await Cielo.saveCartao(cart);
            final snackBar = SnackBar(
              content: Text(salvou["msg"]),
              backgroundColor: salvou["success"] ? Colors.green[400] : Colors.red[400] ,
            );
            _scaffoldKey.currentState.showSnackBar(snackBar);

            if (salvou["success"]){
              Navigator.pop(context);
            }

            },
            backgroundColor: Colors.blue[900],
            icon: Icon(_icon),
            label: Text("Forma de Pagamento")
          ) : Text("")
        );
    }

  Widget _buildPageContent(context, object) {
    return SafeArea(
      child: Column(
        children: <Widget>[
          CreditCardWidget(
            cardNumber: cardNumber,
            expiryDate: expiryDate,
            cardHolderName: cardHolderName,
            cvvCode: cvvCode,
            showBackView: isCvvFocused,
          ),
          Expanded(
            child: SingleChildScrollView(
              child: CreditCardForm(
                onCreditCardModelChange: object.onCreditCardModelChange,
              ),
            )
          ),
        ],
      ),
    );
  }
}