import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:deliveryfarma/models/creditcard.dart';
import 'package:flutter_cielo/flutter_cielo.dart';
class Cielo {
   static CieloEcommerce cielo = CieloEcommerce(
      environment: Environment.SANDBOX, // ambiente de desenvolvimento
      merchant: Merchant(
        merchantId: "2eccfa33-5755-4203-9f51-5b312905fa41",
        merchantKey: "URFETLYHEHUZTGSUNGYGQWQSTOOEPZHPJSKGLQZM",
      ));


  static Future<String> tokerizarCartao (CreditCard card) async {
    try {
      var response = await Cielo.cielo.tokenizeCard(card);
      return response.cardToken;
    } on CieloException catch(e){
      print(e.message);
      print(e.errors[0].message);
      print(e.errors[0].code);
    }

  }
  static  saveCartao(CreditCardModel card) async {
     Map<String, dynamic> result = {"msg": "", "success": false };

     if (card.cardNumber.trim().isEmpty)  return {"msg": "Numero do cartão inválido", "success": false };
     if (card.cvvCode.trim().isEmpty)  return {"msg": "Código de verificação do cartão é obrigatório", "success": false };
     if (card.expiryDate.trim().isEmpty)  return {"msg": "Data de validade é obrigatório", "success": false };
     if (card.cardHolderName.trim().isEmpty)  return {"msg": "Nome no cartão é obrigatório", "success": false };

     card.cardNumber.replaceAll(" ", "");


     await Firestore.instance
         .collection("formas").document().setData(card.toJsonEncrypt()).then((doc) => {
        result = {"msg": "Forma de pagamento salva com sucesso!", "success": true }
     });

     return result;
   }


   static Stream<List<CreditCardModel>> getFormasPagamento(String uidUser) {
     return Firestore.instance
         .collection("formas").where(CreditCardModel.encrypt64("user"), isEqualTo: CreditCardModel.encrypt64(uidUser))
         .snapshots()
         .map((QuerySnapshot snapshot) {
       return snapshot.documents.map((doc) {
         return CreditCardModel.fromDocument(doc);
       }).toList();
     });
   }


}
