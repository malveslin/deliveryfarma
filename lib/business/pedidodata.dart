import 'dart:async';
import 'package:deliveryfarma/models/pedido.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/services.dart';

import 'auth.dart';

class PedidoData {

  static Future<Pedido> getPedido(DocumentReference pedidoReference) async {
    DocumentSnapshot pedidoRef = await pedidoReference.get();
    return Pedido.fromDocument(pedidoRef);
  }

  static DocumentReference getReference(String idPedido) {
    DocumentReference pedidoReference = Firestore.instance.collection('pedidos')
        .document(idPedido);
    return pedidoReference;
  }

  static Stream<List<Pedido>> getPedidos() {
    return Firestore.instance
        .collection("pedidos")
        .snapshots()
        .map((QuerySnapshot snapshot) {
      return snapshot.documents.map((doc) {
        return Pedido.fromDocument(doc);
      }).toList();
    });
  }


  static Future<List<Pedido>> getPedidoFuture() {
    return Firestore.instance
        .collection("pedidos").getDocuments().then((QuerySnapshot snapshot) {
      return snapshot.documents.map((doc) {
        return Pedido.fromDocument(doc);
      }).toList();
    });
  }


  static Future<dynamic> savePedido(Pedido pedido) {
    return Firestore.instance.collection("pedidos").document(pedido.codigo.toString()).setData(
        pedido.toJson()).then((value) {
          return value;
    });
  }

  static Future<List<Pedido>> getPedidoByUserFuture() async {
    FirebaseUser user = await Auth.getCurrentFirebaseUser();
    DocumentReference userRef = Firestore.instance.collection("users").document(
        user.uid);
    return Firestore.instance
        .collection("pedidos").where("user", isEqualTo: userRef)
        .getDocuments()
        .then((QuerySnapshot snapshot) {
      return snapshot.documents.map((doc) {
        return Pedido.fromDocument(doc);
      }).toList();
    });
  }

  static Future<int> getCodigoPedido() async {
    List<Pedido> pedidos =
    await Firestore.instance.collection('pedidos').orderBy("codigo", descending: true)
        .limit(1).getDocuments()
        .then((QuerySnapshot snapshot) {
        return snapshot.documents.map((doc) {
          return Pedido.fromDocument(doc);
        }).toList();
    });
    print(pedidos);
    if (pedidos.length > 0) return pedidos[0].codigo + 1;
    return 1205800;
  }

}
