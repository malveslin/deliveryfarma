import 'dart:async';
import 'package:deliveryfarma/models/produto.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/services.dart';

class ProdutoData {
  static Stream<List<Produto>> getProdutos(DocumentReference farmacia) {
    return Firestore.instance
        .collection("produtos").where("farmacia", isEqualTo: farmacia)
        .snapshots()
        .map((QuerySnapshot snapshot) {
      return snapshot.documents.map((doc) {
        return Produto.fromDocument(doc);
      }).toList();
    });
  }

  static Stream<List<Produto>> getProdutosSearch(DocumentReference farmacia, String query) {
    return Firestore.instance
        .collection("produtos").where("farmacia", isEqualTo: farmacia)
        .snapshots()
        .map((QuerySnapshot snapshot) {
       List<Produto> produtos =  snapshot.documents.map((doc) {
        return Produto.fromDocument(doc);
      }).toList();
       List<Produto> result = [];
       if (query.isNotEmpty){
         produtos.forEach((produto) {
            if (produto.nome.contains(query)){
              result.add(produto);
            }
         });
       }else{
         result = produtos;
       }
       return result;
    });
  }

  static Future<Produto> getProduto(String ID) async {
    Produto produto = new Produto();
     await Firestore.instance
        .collection("produtos").document(ID).get().then((doc) => {
          produto =  Produto.fromDocument(doc)
    });
     return produto;
  }

}
