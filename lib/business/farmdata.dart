import 'dart:async';
import 'package:deliveryfarma/models/farmacia.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/services.dart';

class FarmData {

  static List<Farmacia> farmacias = [];

  static Future<Farmacia> getFarmacia(DocumentReference farmReference) async {
    DocumentSnapshot userRef = await farmReference.get();
    return Farmacia.fromDocument(userRef);
  }

  static DocumentReference getReference(String idFarmacia) {
    DocumentReference farmReference = Firestore.instance.collection('farmacia')
        .document(idFarmacia);
    return farmReference;
  }

  static Stream<List<Farmacia>> getFarmacias() {
    return Firestore.instance
        .collection("farmacia")
        .snapshots()
        .map((QuerySnapshot snapshot) {
      return snapshot.documents.map((doc) {
        return Farmacia.fromDocument(doc);
      }).toList();
    });
  }

  static Future<List<Farmacia>> getFarmaciasFuture() {
    return Firestore.instance
        .collection("farmacia").getDocuments().then((QuerySnapshot snapshot) {
      return snapshot.documents.map((doc) {
        return Farmacia.fromDocument(doc);
      }).toList();
    });
  }

  static Future<List<Farmacia>> getFarmas() async {
    var farmas = await Firestore.instance.collection("farmacia").getDocuments();
    List<Farmacia> farms = [];
    farmas.documents.forEach((f) {
      farms.add(Farmacia.fromDocument(f));
    });
    return farms;
  }

  static Future<List<Farmacia>> setFarmas() async {
    var farmas = await Firestore.instance.collection("farmacia").getDocuments();
    List<Farmacia> farms = [];
    farmas.documents.forEach((f) {
      farmacias.add(Farmacia.fromDocument(f));
    });
  }
}
